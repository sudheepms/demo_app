package com.example.administrator.demo_app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ChangePassword_Activity extends AppCompatActivity {
    private Button _save;
    private EditText _new_password;
    private  EditText _conform_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_);
        _new_password=(EditText)findViewById(R.id.edt_new_pwd);
        _conform_password=(EditText)findViewById(R.id.edt_conform_pwd);
        _save=(Button)findViewById(R.id.btn_save);
        _save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate())
                {

                }
            }
        });

    }
    public boolean validate() {
        boolean valid = true;

        String new_pwd= _new_password.getText().toString();
        String conform_pwd= _conform_password.getText().toString();


        if ( new_pwd.isEmpty() || new_pwd.length() < 6 || new_pwd.length() > 10) {
            _new_password.setError("between 6 and 10 alphanumeric characters");
            valid = false;
        } else {
            _new_password.setError(null);
        }

        if ( new_pwd!=conform_pwd) {
            _conform_password.setError("password not matching");
            valid = false;
        } else {
            _conform_password.setError(null);
        }

        return valid;
    }

}
