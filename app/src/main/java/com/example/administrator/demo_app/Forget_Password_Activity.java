package com.example.administrator.demo_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Forget_Password_Activity extends AppCompatActivity {
    private Button _change_password;
    private Button _send_verif;
    private EditText _input_email;
    private EditText _input_mobile;
    private EditText _input_verifcd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget__password_);
        _input_email=(EditText)findViewById(R.id.edt_email);
        _input_mobile=(EditText)findViewById(R.id.edt_mobile);
        _input_verifcd=(EditText)findViewById(R.id.input_verif_code);
        _change_password=(Button)findViewById(R.id.btn_change_pwd);
        _send_verif=(Button)findViewById(R.id.btn_send_verif);
        _send_verif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate1()) {

                }

                }
        });
        _change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent1 = new Intent(getApplicationContext(), ChangePassword_Activity.class);
                    startActivity(intent1);

            }
        });
    }


    public boolean validate1() {
        boolean valid = true;

        String email = _input_email.getText().toString();
        String mobile = _input_mobile.getText().toString();


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _input_email.setError("enter a valid email address");
            valid = false;
        } else {
            _input_email.setError(null);
        }

        if ( mobile.isEmpty() || mobile.length() < 10 ||  mobile.length() > 10) {
            _input_mobile.setError("enter a valid mobile number");
            valid = false;
        } else {
            _input_mobile.setError(null);
        }

        return valid;
    }

    }
